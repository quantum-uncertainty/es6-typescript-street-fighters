export interface CreateElementOptions {
    tagName: string;
    className?: string;
    attributes?: { [key: string]: string | number };
}

export interface ModalParameters {
    title: string;
    bodyElement: HTMLElement;
    onClose?: () => void;
}