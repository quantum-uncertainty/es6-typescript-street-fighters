export interface Fighter {
  _id: string;
  name: string;
  source: string;
}

export interface FighterDetails extends Fighter {
  health: number;
  attack: number;
  defense: number;
}

export type FighterPosition = 'right' | 'left';

export type SelectedFighters = [FighterDetails, FighterDetails];

export type FighterApiResponse = Fighter[] | FighterDetails;

export type FightersSelector = (event: Event, fighterId: string) => Promise<void>;
