import { callApi } from '../helpers/apiHelper';

import { Fighter, FighterDetails } from '../types/fighter.types';

class FighterService {
  async getFighters() {
    const endpoint = 'fighters.json';
    const apiResult = await callApi(endpoint, 'GET') as Fighter[];
    return apiResult;
  }

  async getFighterDetails(id: string) {
    const endpoint = `details/fighter/${id}.json`;
    const apiResult = await callApi(endpoint, 'GET') as FighterDetails;
    return apiResult;
  }
}

export const fighterService = new FighterService();
