import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { showWinnerModal } from './modal/winner';
import { fight } from './fight';

import { FighterDetails, FighterPosition, SelectedFighters } from '../types/fighter.types';

export function renderArena(selectedFighters: SelectedFighters): void {
  const root = document.getElementById('root') as HTMLElement;
  const arena = createArena(selectedFighters);

  root.innerHTML = '';
  root.append(arena);

  fight(...selectedFighters)
    .then(winner => showWinnerModal(winner as FighterDetails));
}

function createArena(selectedFighters: SelectedFighters) {
  const arena = createElement({ tagName: 'div', className: 'arena___root' });
  const healthIndicators = createHealthIndicators(...selectedFighters);
  const fighters = createFighters(...selectedFighters);

  arena.append(healthIndicators, fighters);
  return arena;
}

function createHealthIndicators(leftFighter: FighterDetails, rightFighter: FighterDetails) {
  const healthIndicators = createElement({ tagName: 'div', className: 'arena___fight-status' });
  const versusSign = createElement({ tagName: 'div', className: 'arena___versus-sign' });
  const leftFighterIndicator = createHealthIndicator(leftFighter, 'left');
  const rightFighterIndicator = createHealthIndicator(rightFighter, 'right');

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter: FighterDetails, position: FighterPosition) {
  const { name } = fighter;
  const container = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
  const fighterName = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  const indicator = createElement({ tagName: 'div', className: 'arena___health-indicator' });
  const bar = createElement({ tagName: 'div', className: 'arena___health-bar', attributes: { id: `${position}-fighter-indicator` } });

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(firstFighter: FighterDetails, secondFighter: FighterDetails) {
  const battleField = createElement({ tagName: 'div', className: `arena___battlefield` });
  const firstFighterElement = createFighter(firstFighter, 'left');
  const secondFighterElement = createFighter(secondFighter, 'right');

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter: FighterDetails, position: FighterPosition) {
  const imgElement = createFighterImage(fighter);
  const positionClassName = `arena___${position}-fighter`;
  const fighterElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`,
  });

  fighterElement.append(imgElement);
  return fighterElement;
}
