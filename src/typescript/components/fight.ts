import { controls } from '../../constants/controls';
import { ArenaFighter } from './arenaFighter';

import { FighterDetails } from '../types/fighter.types';

export async function fight(
  firstFighterDetails: FighterDetails,
  secondFighterDetails: FighterDetails): Promise<FighterDetails> {
  return new Promise(resolve => {
    const fighterOne = new ArenaFighter(firstFighterDetails, 'left');
    const fighterTwo = new ArenaFighter(secondFighterDetails, 'right');

    const pressedKeys: Set<string> = new Set();

    document.addEventListener('keydown', onKeyDown);
    document.addEventListener('keyup', onKeyUp);

    function onKeyDown(event: KeyboardEvent) {
      if (event.repeat) return;
      pressedKeys.add(event.code);
      switch (event.code) {
        case controls.PlayerOneAttack: {
          fighterOne.hit(fighterTwo);
          break;
        }
        case controls.PlayerOneBlock: {
          fighterOne.block();
          break;
        }
        case controls.PlayerTwoAttack: {
          fighterTwo.hit(fighterOne);
          break;
        }
        case controls.PlayerTwoBlock: {
          fighterTwo.block();
          break;
        }
      }
      // handle combinations
      if (combinationPressed(controls.PlayerOneCriticalHitCombination)) {
        fighterOne.hitCritical(fighterTwo);
      }
      if (combinationPressed(controls.PlayerTwoCriticalHitCombination)) {
        fighterTwo.hitCritical(fighterOne);
      }

      if (isGameOver()) {
        const winner = fighterOne.health === 0 ?
          secondFighterDetails :
          firstFighterDetails;
        finishGame(winner);
      }
    }

    function isGameOver() {
      return fighterOne.health === 0 || fighterTwo.health === 0;
    }

    function combinationPressed(combination: string[]) {
      return combination.every(key => pressedKeys.has(key));
    }

    function onKeyUp(event: KeyboardEvent) {
      pressedKeys.delete(event.code);
      if (event.code === controls.PlayerOneBlock) {
        fighterOne.stopBlocking();
      }
      if (event.code === controls.PlayerTwoBlock) {
        fighterTwo.stopBlocking();
      }
    }

    function finishGame(winner: FighterDetails) {
      document.removeEventListener('keydown', onKeyDown);
      document.removeEventListener('keyup', onKeyDown);
      resolve(winner);
    }
  });
}

export function getCriticalDamage(fighter: ArenaFighter): number {
  return fighter.attack * 2;
}

export function getDamage(attacker: ArenaFighter, defender: ArenaFighter): number {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return Math.max(0, damage);
}

export function getHitPower(fighter: ArenaFighter): number {
  const criticalHitChance = getRandomNumber(1, 2);
  const hitPower = fighter.attack * criticalHitChance;
  return hitPower;
}

export function getBlockPower(fighter: ArenaFighter): number {
  const dodgeChance = getRandomNumber(1, 2);
  const blockPower = fighter.defense * dodgeChance;
  return blockPower;
}

function getRandomNumber(min: number, max: number) {
  return Math.random() * (max - min) + min;
}