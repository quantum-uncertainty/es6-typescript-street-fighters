import {
  getCriticalDamage,
  getDamage,
} from './fight';

import { FighterDetails, FighterPosition } from '../types/fighter.types';

export class ArenaFighter {
  attack: number;
  defense: number;
  health: number;
  private initialHealth: number;
  private position: FighterPosition;
  private healthbar: HTMLElement;
  private criticalHitAllowed: boolean;
  private isBlocking: boolean;

  constructor(fighterDetails: FighterDetails, position: FighterPosition) {
    this.attack = fighterDetails.attack;
    this.defense = fighterDetails.defense;
    this.health = fighterDetails.health;
    this.initialHealth = fighterDetails.health;
    this.position = position;
    this.healthbar = document.getElementById(`${this.position}-fighter-indicator`) as HTMLElement;
    this.criticalHitAllowed = true;
    this.isBlocking = false;
  }

  private get canHit() {
    return !this.isBlocking;
  }

  private get canHitCritical() {
    return this.canHit && this.criticalHitAllowed;
  }

  hit(defender: ArenaFighter): void {
    if (this.canHit) {
      const damage = defender.isBlocking ? 0 : getDamage(this, defender);
      defender.applyDamage(damage);
    }
  }

  hitCritical(defender: ArenaFighter): void {
    const CRITICAL_HIT_RESTORE_TIME = 10000;
    const restoreCriticalHitAbility = () => {
      this.criticalHitAllowed = true;
    };
    if (this.canHitCritical) {
      const damage = getCriticalDamage(this);
      defender.applyDamage(damage);
      this.criticalHitAllowed = false;
      setTimeout(restoreCriticalHitAbility, CRITICAL_HIT_RESTORE_TIME);
    }
  }

  block(): void {
    this.isBlocking = true;
  }

  stopBlocking(): void {
    this.isBlocking = false;
  }

  applyDamage(damage: number): void {
    this.health = Math.max(0, this.health - damage);
    this.redrawHealthbar();
  }

  private redrawHealthbar() {
    const width = (this.health / this.initialHealth) * 100;
    this.healthbar.style.width = width + '%';
  }
}