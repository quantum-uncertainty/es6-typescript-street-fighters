import { createElement, createElementWithText } from '../helpers/domHelper';

import { FighterDetails, FighterPosition } from '../types/fighter.types';

export function createFighterPreview(fighter: FighterDetails, position: FighterPosition): HTMLElement {
  const positionClassName = `arena___${position}-fighter`;
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  const fighterImage = createFighterImage(fighter);
  const fighterDetails = createFighterDetails(fighter);
  fighterElement.append(fighterImage, ...fighterDetails);
  return fighterElement;
}

function createFighterDetails(fighter: FighterDetails) {
  const options = {
    tagName: 'p',
    className: 'fighter-preview___text',
  };
  const nameEl = createElementWithText(options, `Name: ${fighter.name}`);
  const healthEl = createElementWithText(options, `Health: ${fighter.health}`);
  const attackEl = createElementWithText(options, `Attack: ${fighter.attack}`);
  const defenseEl = createElementWithText(options, `Defense: ${fighter.defense}`);
  return [nameEl, healthEl, attackEl, defenseEl];
}

export function createFighterImage(fighter: FighterDetails): HTMLElement {

  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
