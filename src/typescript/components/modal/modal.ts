import { createElement } from '../../helpers/domHelper';
import { ModalParameters } from '../../types/dom.types';

export function showModal(modalParams: ModalParameters): void {
  const root = getModalContainer();
  const modal = createModal(modalParams);

  root.append(modal);
}

function getModalContainer() {
  return document.getElementById('root') as HTMLElement;
}

function createModal({ title, bodyElement, onClose }: ModalParameters) {
  const layer = createElement({ tagName: 'div', className: 'modal-layer' });
  const modalContainer = createElement({ tagName: 'div', className: 'modal-root' });
  const header = createHeader(title, onClose);

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
}

// eslint-disable-next-line @typescript-eslint/no-empty-function
function createHeader(title: string, onClose: (() => void) = () => { }) {
  const headerElement = createElement({ tagName: 'div', className: 'modal-header' });
  const titleElement = createElement({ tagName: 'span' });
  const closeButton = createElement({ tagName: 'div', className: 'close-btn' });

  titleElement.innerText = title;
  closeButton.innerText = '×';

  const close = () => {
    hideModal();
    onClose();
  }
  closeButton.addEventListener('click', close);
  headerElement.append(titleElement, closeButton);

  return headerElement;
}

function hideModal() {
  const modal = document.getElementsByClassName('modal-layer')[0];
  modal?.remove();
}
