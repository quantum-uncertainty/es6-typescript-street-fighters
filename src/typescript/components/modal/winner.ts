import { createElement, createElementWithText } from '../../helpers/domHelper';
import { showModal } from './modal';

import { FighterDetails } from '../../types/fighter.types';

export function showWinnerModal(fighter: FighterDetails): void {
  const bodyElement = createElement({ tagName: 'div', className: 'modal-body' });
  const textElement = createElementWithText({ tagName: 'span' }, `${fighter.name} won!`);
  bodyElement.append(textElement);
  showModal({ title: 'Game over', bodyElement });
}
